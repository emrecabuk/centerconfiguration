﻿using ConfCenter.Extensions;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConfCenter.Models
{
    public class ConfigurationModel : MongoBaseModel
    {
        [BsonElement("Name")]
        [Required(ErrorMessage = "İsim boş geçilemez.")]
        public string Name { get; set; }

        [BsonElement("Type")]
        [Required(ErrorMessage = "Tip boş geçilemez.")]
        public ValueEnum Type { get; set; }

        [BsonElement("Value")]
        [Required(ErrorMessage = "Değer boş geçilemez.")]
        public string Value { get; set; }

        [BsonElement("ApplicationName")]
        [Required(ErrorMessage = "Uygulama Adı boş geçilemez.")]
        public string ApplicationName { get; set; }
        public string TypeName => Type.GetDescription();
    }
}
