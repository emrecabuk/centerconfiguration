﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfCenter.Models
{
    public enum Status : byte
    {
        Deleted = 0,
        Active = 1,
    }
}
