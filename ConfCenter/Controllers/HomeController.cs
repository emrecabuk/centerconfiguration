﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConfCenter.Models;
using Microsoft.AspNetCore.Authorization;
using ConfCenter.Repository;
using MongoDB.Bson;
using ConfCenter.Repository.Abstract;
using AutoMapper;
using ConfCenter.Entity;
using System.Linq.Expressions;

namespace ConfCenter.Controllers
{
    public class HomeController :  Controller
    {
        private IConfigurationRepository _repository;
        private IMapper _mapper;

        public HomeController(IConfigurationRepository repository,IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        
        public async Task<IActionResult> Index()
        {
            var entityList=await _repository.GetListAsync();
            var result = _mapper.Map<List<ConfigurationEntity>, List<ConfigurationModel>>(entityList);
            return View(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("post")]
        public async Task<IActionResult> Create(string id)
        {
            ConfigurationModel confModel = new ConfigurationModel();
            if (!string.IsNullOrEmpty(id))
                confModel= _mapper.Map<ConfigurationEntity,ConfigurationModel>(await _repository.GetByIdAsync(new ObjectId(id)));
            
            return View(confModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("post")]
        public async Task<IActionResult> Create(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                try
                {
                    Expression<Func<ConfigurationEntity, bool>> predicate = x => x.Name.Equals(model.Name) && x.ApplicationName.Equals(model.ApplicationName) && x.Status == Status.Active;
                    var checkEntity=await _repository.GetByPredicateAsync(predicate);
                    if (checkEntity != null)
                    {
                        ViewBag.Message = "Aynı servis adına ve isime sahip aktif kayıt bulunmaktadır.";
                        return View(model);
                    }
                    var entity=_mapper.Map<ConfigurationModel, ConfigurationEntity>(model);
                    entity.Id = ObjectId.GenerateNewId();
                    entity.CreateDate = DateTime.UtcNow;
                    entity.UpdateDate = DateTime.UtcNow;
                    entity.Status = Status.Active;
                    await _repository.CreateAsync(entity);
                    ViewBag.Message = "Başarıyla eklenmiştir.";
                    return View(model);
                }
                catch (System.Exception ex)
                {
                    ViewBag.Message = "Herhangi bir sorun ile karşılaşılmıştır.";
                    return View(model);
                }
            } 
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("update")]
        public async Task<IActionResult> Update(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                try
                {
                    var tempEntity=await _repository.GetByIdAsync(new ObjectId(model.Id));
                    if (tempEntity == null)
                    {
                        ViewBag.Message = "İlgili kayıt bulunamamıştır.";
                        return View(model);
                    }
                    tempEntity.ApplicationName = model.ApplicationName;
                    tempEntity.Name = model.Name;
                    tempEntity.Value = model.Value;
                    tempEntity.Type = model.Type;
                    tempEntity.UpdateDate = DateTime.UtcNow;
                    await _repository.UpdateAsync(tempEntity);
                    ViewBag.Message = "Başarıyla güncellenmiştir.";
                    return RedirectToAction("Index", "Home");
                }
                catch (System.Exception ex)
                {
                    ViewBag.Message = "Herhangi bir sorun ile karşılaşılmıştır.";
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("delete")]
        public async Task<IActionResult> Delete(string id)
        {
                try
                {
                    var tempEntity = await _repository.GetByIdAsync(new ObjectId(id));
                    if (tempEntity == null)
                    {
                        ViewBag.Message = "İlgili kayıt bulunamamıştır.";
                        return RedirectToAction("Index", "Home");
                    }
                    await _repository.DeleteAsync(tempEntity);
                    ViewBag.Message = "Başarıyla silinmiştir.";
                    return RedirectToAction("Index", "Home");
                }
                catch (System.Exception ex)
                {
                    ViewBag.Message = "Herhangi bir sorun ile karşılaşılmıştır.";
                    return RedirectToAction("Index", "Home");
                }
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
