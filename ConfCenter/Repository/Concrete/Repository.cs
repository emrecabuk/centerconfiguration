﻿using ConfCenter.Entity;
using ConfCenter.Models;
using ConfCenter.Repository.Abstract;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ConfCenter.Repository.Concrete
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : MongoBaseEntity, new()
    {
        private readonly IMongoCollection<TEntity> mongoCollection;
        private readonly string _mongoDBConnectionString;
        private readonly string _dbName;
        private readonly string _collectionName;

        public Repository(string mongoDBConnectionString, string dbName, string collectionName)
        {
            _mongoDBConnectionString = mongoDBConnectionString;
            _dbName = dbName;
            _collectionName = collectionName;
            var client = new MongoClient(_mongoDBConnectionString);
            var database = client.GetDatabase(_dbName);
            mongoCollection = database.GetCollection<TEntity>(_collectionName);
        }

        public virtual List<TEntity> GetList()
        {
            return mongoCollection.Find(m => m.Status == Status.Active).ToList();
        }

        public async virtual Task<List<TEntity>> GetListAsync()
        {
            return await mongoCollection.Find(m => m.Status == Status.Active).ToListAsync();
        }

        public virtual List<TEntity> GetListByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            return mongoCollection.Find(predicate).ToList();
        }

        public async virtual Task<List<TEntity>> GetListByPredicateAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await mongoCollection.Find(predicate).ToListAsync();
        }

        public async virtual Task<TEntity> GetByIdAsync(ObjectId id)
        {
            return await mongoCollection.Find<TEntity>(m => m.Id == id && m.Status == Status.Active).FirstOrDefaultAsync();
        }

        public async virtual Task<TEntity> GetByPredicateAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await mongoCollection.Find(predicate).FirstOrDefaultAsync();
        }

        public virtual TEntity GetById(ObjectId id)
        {
            return mongoCollection.Find<TEntity>(m => m.Id == id && m.Status == Status.Active).FirstOrDefault();
        }

        public virtual TEntity GetByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            return mongoCollection.Find(predicate).FirstOrDefault();
        }
        public async virtual Task<TEntity> CreateAsync(TEntity model)
        {
            await mongoCollection.InsertOneAsync(model);
            return model;
        }

        public async virtual Task<TEntity> UpdateAsync(TEntity model)
        {
            await mongoCollection.ReplaceOneAsync(m => m.Id == model.Id, model);
            return model;
        }

        public virtual void Update(string id, TEntity model)
        {
            var docId = new ObjectId(id);
            mongoCollection.ReplaceOne(m => m.Id == docId, model);
        }

        public async virtual Task DeleteAsync(TEntity model)
        {
            model.Status = Status.Deleted;
            await mongoCollection.ReplaceOneAsync(m => m.Id == model.Id, model);
        }

        public virtual void Delete(TEntity model)
        {
            mongoCollection.DeleteOne(m => m.Id == model.Id);
        }

        public virtual void Delete(string id)
        {
            var docId = new ObjectId(id);
            mongoCollection.DeleteOne(m => m.Id == docId);
        }
    }
}
