﻿using ConfCenter.Entity;
using ConfCenter.Models;
using ConfCenter.Repository.Abstract;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ConfCenter.Repository.Concrete
{
    public class ConfigurationRepository : Repository<ConfigurationEntity>, IConfigurationRepository
    {
        public ConfigurationRepository(string mongoDBConnectionString, string dbName, string collectionName) : base(mongoDBConnectionString, dbName, collectionName)
        {
        }
    }
}
