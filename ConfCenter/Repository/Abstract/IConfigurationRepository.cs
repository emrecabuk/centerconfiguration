﻿using ConfCenter.Entity;
using ConfCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfCenter.Repository.Abstract
{
    public interface IConfigurationRepository : IRepository<ConfigurationEntity>
    {
    }
}
