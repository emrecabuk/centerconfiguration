﻿using ConfCenter.Entity;
using ConfCenter.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ConfCenter.Repository.Abstract
{
    public interface IRepository<TEntity> where TEntity : MongoBaseEntity
    {
       List<TEntity> GetList();
       Task<List<TEntity>> GetListAsync();
       List<TEntity> GetListByPredicate(Expression<Func<TEntity, bool>> predicate);
       Task<List<TEntity>> GetListByPredicateAsync(Expression<Func<TEntity, bool>> predicate);
       Task<TEntity> GetByIdAsync(ObjectId id);
       Task<TEntity> GetByPredicateAsync(Expression<Func<TEntity, bool>> predicate);
       TEntity GetById(ObjectId id);
       TEntity GetByPredicate(Expression<Func<TEntity, bool>> predicate);
       Task<TEntity> CreateAsync(TEntity model);
       Task<TEntity> UpdateAsync(TEntity model);
       Task DeleteAsync(TEntity model);
        void Update(string id, TEntity model);
       void Delete(TEntity model);
       void Delete(string id);
    }
}
