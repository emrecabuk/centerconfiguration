﻿using ConfCenter.Repository.Abstract;
using ConfCenter.Repository.Concrete;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfCenter.Infrastructure
{
    public static class AppServices
    {
        public static IServiceCollection AddAppicationServices(this IServiceCollection services, string mongoConnectionString, string dbName, string collectionName)
        {
            services.AddScoped<IConfigurationRepository>(x => new ConfigurationRepository(mongoConnectionString,dbName,collectionName));

            return services;
        }
    }
}
