﻿using AutoMapper;
using ConfCenter.Entity;
using ConfCenter.Models;
using System;

namespace ConfCenter.Infrastructure
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() : this("AutoMapperProfileMappings")
        {
        }
        public AutoMapperProfile(string profileName) : base(profileName)
        {
            CreateMap<ConfigurationEntity, ConfigurationModel>();
            CreateMap<ConfigurationModel, ConfigurationEntity>();

            CreateMap<MongoBaseEntity, MongoBaseModel>();
            CreateMap<MongoBaseModel, MongoBaseEntity>();
        }
    }
}
