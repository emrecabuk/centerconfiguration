﻿using ConfCenter.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfCenter.Entity
{
    public class MongoBaseEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("Status")]
        public Status Status { get; set; }

        [BsonElement("CreateDate")]
        public DateTime CreateDate { get; set; }

        [BsonElement("UpdateDate")]
        public DateTime? UpdateDate { get; set; }
    }
}
